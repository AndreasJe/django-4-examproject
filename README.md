![Header image](assets/logo_git.png)

# Exam Project for Python/Django Elective 2022

Project introduction

## Description

Project description

## How to run

-  Navigate to work directory
-  Run the following command: python manage.py runserver
-  Project can be accessed at [localhost:8000](http://127.0.0.1:8000/)

```
Watching for file changes with StatReloader
Performing system checks...

System check identified no issues (0 silenced).
May 04, 2022 - 23:11:11
Django version 4.0.4, using settings 'exam_project.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CONTROL-C. 
```

## Requirements


Dependencies are found in the [Requirements.txt](https://gitlab.com/AndreasJe/django-4-examproject/blob/main/requirements.txt)

## Team

- [Nikolai Baida](https://gitlab.com/niko856)
- [Victor Øbro Bucholdtz](https://gitlab.com/vict889f)
- [Andreas Jensen](https://gitlab.com/AndreasJe)
