
from rest_framework import generics, permissions
from rest_framework.response import Response
from .models import Account
from .serializers import AccountSerializer
from .permissions import IsOwnerOrNoAccess


class AccountList(generics.ListCreateAPIView):
    queryset = Account.objects.all()
    serializer_class = AccountSerializer


class AccountDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Account.objects.all()
    serializer_class = TransferSerializer
