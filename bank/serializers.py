from rest_framework import serializers
from .models import Account, Ledger


class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('user', 'name', 'createdAt')
        model = Account
