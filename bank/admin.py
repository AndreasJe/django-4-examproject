from django.contrib import admin
from .models import Rank, BankCustomer, Account, Ledger, ExternalLedger

admin.site.register(Rank)
admin.site.register(BankCustomer)
admin.site.register(Account)
admin.site.register(Ledger)
admin.site.register(ExternalLedger)
